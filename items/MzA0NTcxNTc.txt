◎ 片名: 蓝皮书 第二季 Project Blue Book Season 2

◎ 编剧: 大卫·奥莱利 / Emily Brochin

◎ 类型: 剧情 / 科幻 / 悬疑 / 惊悚

◎ 官方网站: https://www.history.com/shows/project-blue-book/season-2

◎ 制片国家/地区: 美国

◎ 语言: 英语

◎ 首播: 2020-01-21(美国)

◎ 集数: 10

◎ 又名: 蓝皮书

◎ IMDB 链接： https://www.imdb.com/title/tt9758092

◎ 剧情: 
历史频道的外星人UFO题材悬疑剧集《蓝皮书计划》续订第2季，仍为10集。
 

◎ 豆瓣链接 (8.0): https://movie.douban.com/subject/30457157/

================ 资源 ================ 

## GD 

https://drive.google.com/drive/folders/10Sz-mHkP-y2b-IyxP6-ydeIzUNBeg54Z


