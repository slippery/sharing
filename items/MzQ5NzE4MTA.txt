◎ 片名: 养虎为患 Tiger King

◎ 导演: Rebecca Chaiklin / Eric Goode

◎ 类型: 纪录片

◎ 制片国家/地区: 美国

◎ 语言: 英语

◎ 首播: 2020-03-20(美国)

◎ 集数: 7

◎ 单集片长: 50分钟

◎ 又名: 虎王 / Tiger King: Murder, Mayhem and Madness

◎ IMDB 链接： https://www.imdb.com/title/tt11829330

◎ 剧情: 
在这部限定纪录片剧集中，备受争议的动物公园老板“乔”卷入了一场雇凶杀人的阴谋，老虎之间的竞争恶化，但更危险的是老虎的主人。
 

◎ 豆瓣链接 (8.0): https://movie.douban.com/subject/34971810/

================ 资源 ================ 
## 1080 多国语言 18G 

magnet:?xt=urn:btih:8ba122c28bd06ebe168808339752e5ba6cbe1522

