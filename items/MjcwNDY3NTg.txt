◎ 片名: 逃离比勒陀利亚 Escape from Pretoria

◎ 导演: Francis Annan

◎ 编剧: Francis Annan / L.H. Adams / Karol Griffiths / Tim Jenkin

◎ 类型: 剧情 / 惊悚 / 犯罪

◎ 制片国家/地区: 英国 / 澳大利亚

◎ 语言: 英语

◎ 上映日期: 2020-03-06(英国)

◎ 片长: 106分钟

◎ IMDB 链接： https://www.imdb.com/title/tt5797184

◎ 剧情: 
丹尼尔·雷德克里夫将出演由Francis Annan自编自导的新片[逃离比勒陀利亚](Escape From Pretoria，暂译)。电影根据真实事件改编。丹尼尔将扮演蒂姆·詹金，是两名南非白人中的一员。他们1978年因为参加非洲人民大会秘密反种族隔离行动而被视为恐怖分子并入狱，并策划越狱活动。
 

◎ 豆瓣链接 (7.4): https://movie.douban.com/subject/27046758/

1080p.英文字幕: magnet:?xt=urn:btih:FBED71B82F69E4E3AC89FEE76ADE95E633D075DC

